import { useContext, useEffect } from 'react';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

export default function Logout() {

	// consume the UserContext object and destructure it to access the user state and unsetUser function from context procider.
	const { unsetUser, setUser } = useContext(UserContext);

		// localStorage.clear()
	// Clear the localstorage of the user's information
	unsetUser();

	useEffect(() => {
		setUser({email: null});
	})

	return(
		<Navigate to="/login" />

	)
};



